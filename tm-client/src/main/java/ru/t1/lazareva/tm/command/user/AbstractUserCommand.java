package ru.t1.lazareva.tm.command.user;

import org.jetbrains.annotations.NotNull;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import ru.t1.lazareva.tm.api.endpoint.IAuthEndpoint;
import ru.t1.lazareva.tm.api.endpoint.IUserEndpoint;
import ru.t1.lazareva.tm.command.AbstractCommand;
import ru.t1.lazareva.tm.dto.model.UserDto;
import ru.t1.lazareva.tm.exception.entity.UserNotFoundException;

@Component
public abstract class AbstractUserCommand extends AbstractCommand {

    @NotNull
    @Autowired
    public IUserEndpoint userEndpoint;

    @NotNull
    @Autowired
    public IAuthEndpoint authEndpoint;

    protected void showUser(final UserDto user) {
        if (user == null) throw new UserNotFoundException();
        System.out.println("ID: " + user.getId());
        System.out.println("LOGIN: " + user.getLogin());
    }

    @Override
    public String getArgument() {
        return null;
    }

}