package ru.t1.lazareva.tm.repository.dto;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Repository;
import ru.t1.lazareva.tm.api.repository.dto.ISessionDtoRepository;
import ru.t1.lazareva.tm.dto.model.SessionDto;

import javax.persistence.EntityManager;

@Repository
@Scope("prototype")
@NoArgsConstructor
public class SessionDtoRepository extends AbstractUserOwnedDtoRepository<SessionDto> implements ISessionDtoRepository {

    @Override
    protected Class<SessionDto> getEntityClass() {
        return SessionDto.class;
    }

}
