package ru.t1.lazareva.tm.repository.dto;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Repository;
import ru.t1.lazareva.tm.api.repository.dto.ITaskDtoRepository;
import ru.t1.lazareva.tm.dto.model.TaskDto;

import javax.persistence.EntityManager;
import java.util.List;

@Repository
@Scope("prototype")
@NoArgsConstructor
public final class TaskDtoRepository extends AbstractUserOwnedDtoRepository<TaskDto> implements ITaskDtoRepository {

    @Override
    protected Class<TaskDto> getEntityClass() {
        return TaskDto.class;
    }

    @NotNull
    @Override
    public TaskDto create(
            @NotNull final String userId,
            @NotNull final String name,
            @NotNull final String description
    ) throws Exception {
        @NotNull final TaskDto task = new TaskDto(name, description);
        return add(userId, task);
    }

    @NotNull
    @Override
    public TaskDto create(@NotNull final String userId, @NotNull final String name) throws Exception {
        @NotNull final TaskDto task = new TaskDto(name);
        return add(userId, task);
    }

    @Nullable
    @Override
    public List<TaskDto> findAllByProjectId(@NotNull final String userId, @NotNull final String projectId) throws Exception {
        @NotNull final String jpql = "SELECT m FROM " + getEntityClass().getSimpleName() + " m WHERE m.userId = :userId AND m.projectId = :projectId";
        return entityManager.createQuery(jpql, getEntityClass())
                .setParameter("userId", userId)
                .setParameter("projectId", projectId)
                .getResultList();
    }

}
